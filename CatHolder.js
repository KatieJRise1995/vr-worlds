import React from 'react';
import {asset, View, Model, VrButton} from 'react-vr';

export default class CatHolder extends React.Component {
    render() {
        return (
            <View>
                <Model source={{
                    obj: asset('/cat.obj'),
                    mtl: asset('/cat.mtl')
                }} 
                style={{
                    transform: [
                      {translate: [750, 10, -300]}
                    ]
                  }}
                  texture={"https://live.staticflickr.com/7160/6730861933_1c49ed1a92_b.jpg" }
                  wireframe={true}
                />
            </View>
        )
    }
}