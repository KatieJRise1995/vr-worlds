import React from 'react';
import {  AppRegistry, Animated, AmbientLight, AsyncStorage, asset, Pano, Model, Sound, Text, View, VrButton,} from 'react-vr';
import { Easing } from 'react-native';
import CatHolder from './CatHolder.js';

export default class HelloVirtualWorld extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
    background: 'SpaceMyDudes.jpg',
    sound: '07 Orion.m4a',
    visible: true,  
    spin: new Animated.Value(0) 
    };
  }

  //This will change the scene of the project and the music...
  changeScene() {
    this.setState ({
      background: 'MountainSnow.jpg',
      sound: '08 Vultures.m4a',
      visible: false
    })
  }
  refresh() {
    this.setState ({
      background: 'SpaceMyDudes.jpg',
      sound: '07 Orion.m4a',
      visible: true
    })
  }

  componentDidMount() {
    this.spinAnimation();
  }
  
  spinAnimation() {
    this.state.spin.setValue(0);
    Animated.timing(
      this.state.spin,
      {
       toValue: 1,
       duration: 3000,
       easing: Easing.linear
      }
    ).start( () => this.spinAnimation() );
  }
  
  renderItems(){
    if (this.state.visible === true)
    {
      return <CatHolder />
    }
    else {
      console.log = "I sees you boi"
    }
  }
  
  render() {
    const spin = this.state.spin.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    });

    var AnimatedModel = Animated.createAnimatedComponent(Model);
    return (
      <View>
        <Pano source={asset(this.state.background)}/>
        <Sound 
          loop={true}
          source={{
            wav: asset(this.state.sound)
          }}
        />
        <VrButton onClick={this.changeScene.bind(this)} >
        <Text
          style={{
            backgroundColor: 'transparent',
            color: 'yellow',
            fontSize: 0.5,
            fontWeight: '400',
            layoutOrigin: [0.5, 0.5],
            paddingLeft: 0.2,
            paddingRight: 0.2,
            textAlign: 'center',
            textAlignVertical: 'center',
            transform: [{translate: [0, 0, -5]}],
          }}>
          Basically, my universe!
        </Text>
        </VrButton>
        <AnimatedModel
          source={{
            obj: asset('space-invader.obj'),
          }}
          style={{
            transform: [
              {translate: [0, -4, -4]},
              {rotateY: spin}
            ]
          }}
          texture={"https://live.staticflickr.com/1473/26380152011_86e3f34311_k.jpg" }
          wireframe={false}
        />
        {this.renderItems()}
      </View>
    );
  }
};
AppRegistry.registerComponent('HelloVirtualWorld', () => HelloVirtualWorld);